export default {
  cjs: { type: 'babel', lazy: false },
  esm: { type: 'babel' },
  disableTypeCheck: false
};
