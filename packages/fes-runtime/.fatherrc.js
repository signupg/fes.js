export default {
  target: 'browser',
  cjs: { type: 'babel', lazy: false },
  esm: { type: 'babel' },
  disableTypeCheck: false,
};
